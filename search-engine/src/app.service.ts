import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { compareTwoStrings } from 'string-similarity';
import * as fs from 'fs';

@Injectable()
export class AppService {
  private corpus: string[] = [];

  constructor() {
    const text = fs.readFileSync('src/assets/hemingway.txt', 'utf-8');
    this.corpus = text.split(/[^\w]/).filter((word) => word.length > 0);
  }

  search(word: string, maxCount = 3) {
    const results: Set<string> = new Set();

    const sorted = this.corpus.sort(
      (a, b) => compareTwoStrings(b, word) - compareTwoStrings(a, word),
    );

    sorted.forEach((word) => {
      if (results.size === maxCount) return;

      if (!results.has(word)) {
        results.add(word);
      }
    });

    return Array.from(results);
  }

  addWord(word: string) {
    this.corpus.push(word);
    return { status: HttpStatus.OK };
  }

  removeWord(word: string) {
    if (word) {
      const tempList = this.corpus.filter((item) => item !== word);
      this.corpus = tempList;

      return { status: HttpStatus.OK };
    }

    throw new BadRequestException('Not found the most similar word');
  }
}
