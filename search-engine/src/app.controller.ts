import { Controller, Get, Post, Param, Delete, Res } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get(':word')
  search(@Param('word') word: string): string[] {
    return this.appService.search(word);
  }

  @Post(':word')
  addWord(@Param('word') word: string) {
    return this.appService.addWord(word);
  }

  @Delete(':word')
  removeWord(@Param('word') word: string) {
    return this.appService.removeWord(word);
  }
}
