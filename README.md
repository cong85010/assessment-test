## Description
Search Engine.

## Installation

```bash
$ yarn install
```

## Running the app

```bash
$ yarn start
```

## Tesing

Case 1: 
params: 'word' => 
- word
- sword
- work

Case 2: 
delete: 'word' =>
- sword
- work

Case 3:
add: 'hello' =>
- hello
- sword
- work