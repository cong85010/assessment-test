
export type Action =
  | { type: 'SAVE'; payload: Array<string> }
  | { type: 'ADD'; payload: string }
  | { type: 'DELETE'; payload: string }
  | { type: 'CLEAR'; payload: number };

export interface State {
  items: string[];
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'SAVE':
      return { items: action.payload};
    case 'ADD':
      return { items: [action.payload, ...state.items]};
    case 'DELETE': {
      const index = state.items.indexOf(action.payload);
      console.log('====================================');
      console.log(index);
      console.log('====================================');
      if(index !== -1) {
        const tempList = state.items;
        tempList.splice(index, 1);
        return { items: tempList};
      }
      return { items: state.items};
    }
    case 'CLEAR':
      return {
        items: [],
      };
    default:
      return state;
  }
}