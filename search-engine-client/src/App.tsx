import React, { FormEvent } from 'react';
import './App.css';
import { State, reducer } from './reducer';
import axios from 'axios';
import { FaTrashAlt, FaSearch, FaPlus } from 'react-icons/fa';

const initialState: State = {
  items: [],
};

function App() {
  const [state, dispatch] = React.useReducer(reducer, initialState);

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget);
    const params = formData.get('params');

    if(params) {
      const response = await axios.get(params as string)
      
      dispatch({
        type: 'SAVE',
        payload: response.data
      })
    }
    
  }

  const handleDelete = async (word: string) => {
    try {
      await axios.delete(word)
      dispatch({
        type: 'DELETE',
        payload: word
      })
    } catch (error) {
      console.log(error)
    }
  }

  const handleAdd = async () => {
    try {
      const word = (document.getElementById('wordID') as HTMLInputElement).value;
      if(word) {
        await axios.post(word)

        dispatch({
          type: 'ADD',
          payload: word
        });

        (document.getElementById('wordID') as HTMLInputElement).value = ''
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div className="app">
      <form className='app__form' onSubmit={handleSubmit}>
          <input placeholder='Tìm kiếm' id='wordID' name='params' className='app__form__input' />
          <button title='Tìm từ' type='submit' className='app__btn app__form__btn'><FaSearch /></button>
          <button onClick={handleAdd} title='Thêm từ' type='button' className='app__btn app__btn__add'><FaPlus  /></button>
      </form>
      <ul className='app__list'>
          {
            state.items.map(word => <li key={word} className='app_list__item'><span>{word}</span><FaTrashAlt onClick={() => handleDelete(word)} title='Xoá tìm kiếm' className='icon'/></li>)
          }
      </ul>
    </div>
  );
}

export default App;
